module.exports = {
  purge: [ './src/**/*.html', './src/**/*.vue', './src/**/*.jsx', ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'bears': {
          'blue': '#1A4775',
          'blue-dark': '#112E4B',
          'black': '#151515',
          'white': '#F1F1F1',
          'grey-light': '#A9A9AF',
          'grey-dark': '#262729',
          'yellow': '#E4C603',
          'yellow-dark': '#7F6F02',
        }
      },
      screens: {
        'xs': '475px',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}