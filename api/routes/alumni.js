var express = require('express');
var router = express.Router();
var crypto = require('crypto');

const dbc = require('./../controllers/db');

/* GET tickets listing. */
router.get('/', function(req, res, next) {

    dbc.db.many('SELECT * from tickets')
    .then(function (data) {
        res.send(JSON.stringify(data));
    })
    .catch(function (error) {
        res.send(JSON.stringify([]));
        console.log('ERROR:', error)
  })
});

router.post('/', function(req, res, next) {

    let td = req.body.td;
    let fd = req.body.fd;
    let id = crypto.randomBytes(20).toString('hex');
    let date = new Date(Date.now()+(1000*60*(-(new Date()).getTimezoneOffset()))).toISOString().replace('T',' ').replace('Z','');

    let tableData = {
        'id': id,
        'date': date,
        'ticket_data': td,
        'form_data': fd,
        'status': 'Pending',
    }
    const cs = new dbc.pgp.helpers.ColumnSet(['id','date', 'ticket_data', 'form_data', 'status',], {table: 'tickets'});
    const insert = dbc.pgp.helpers.insert(tableData, cs);

    dbc.db.none(insert)
    .then(() => {
        res.json({error_code:0,err_desc:null, data: id}); 
    })
    .catch(error => {
        res.json({error_code:1,err_desc:"DB Insert Failed", data: error});
    });   
   
});


router.post('/update', function(req, res, next) {
        
    let id = req.body.id;
    let status = req.body.status;

    let ud = { id: id, status: status };

    const condition = dbc.pgp.as.format(' WHERE id = ${id}', ud);
    const update = dbc.pgp.helpers.update(ud, ['status'], 'tickets') + condition;

    dbc.db.none(update)
    .then(() => {
        res.json({error_code:0,err_desc:null, data: 'success'}); 
    })
    .catch(error => {
        res.json({error_code:1,err_desc:"DB Update Failed", data: error});
    });
   
});



router.post('/password', function(req, res, next) {
        
    let pwd = req.body.password;

    if(pwd == 'Skatehard123!')
    {
        dbc.db.many('SELECT * from tickets')
            .then(function (data) {
                res.send(JSON.stringify(data));
            })
            .catch(function (error) {
                res.send(JSON.stringify([]));
                console.log('ERROR:', error)
        })
    }
    else
    {
        res.json({error_code:1,err_desc:"Password Incorrect"});
    }
   
});


module.exports = router;
