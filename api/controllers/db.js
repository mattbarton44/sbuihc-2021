
const pgp = require('pg-promise')(/* options */)
const db = pgp('postgres://username:password@localhost:5432/database'); // sanatized for VC


module.exports = {
    db: db,
    pgp: pgp
}